const fetch = require('node-fetch')
const apiKey = 'k_9nmwyi56'
const getMovieByTitle = async (title) => {
    try {
        fetch(`https://imdb-api.com/API/SearchTitle/${apiKey}/${title}`)
            .then(res => console.log(res.json()))
            .then(parsed => {
                return parsed
            })
    } catch (error) {
        console.log(error)
    }
}
console.log('hi')
const getMovieTrailerById = async (movieId) => {
    try {
        fetch(`https://imdb-api.com/API/Trailer/${apiKey}/${movieId}`)
            .then(res => res.json())
            .then(json => console.log(json))
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    'getMovieByTitle' : getMovieByTitle,
    'getMovieTrailerById' : getMovieTrailerById
}
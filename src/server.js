const express = require('express')
const app = express()
const movieApi = require('./RetrieveMovies')
async function run() {
    await movieApi.getMovieByTitle('aladdin')
        .then(data => console.log(data))
    // const id = json.results[0].id
    // movieApi.getMovieTrailerById(id)
}
run()
app.use('/', express.static('public'))

app.listen(3000, () => {
    console.log("Server has started at http://localhost:3000")
})